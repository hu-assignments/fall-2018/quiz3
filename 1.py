
# This program
# a) performs data cleaning process to remove missing attribute values present in the database.
# b) calculates probability of being breast cancer of an imaginary patient
#    by evaluating his/her sample results provided as command-line argument.

import re
from sys import argv as arguments

data_file = open('WBC.data', 'r').read()

data_dic = {i.split(',')[0]: i.split(',')[1:] for i in data_file.split('\n')}


class ArgumentHandler:
    REGEX = r"\?|(<:\d)|(<=:\d)|(>=:\d)|(>:\d)|(=:\d)|(!=:\d)"

    class IllegalArgumentException(Exception):
        def __init__(self, message: str) -> None:
            self.message = message

    class NumberOfArgumentsException(Exception):
        def __init__(self, message: str) -> None:
            self.message = message

    def __init__(self, arguments: list) -> None:
        self.__arguments = arguments
        try:
            self.check_argument_validity()
        except ArgumentHandler.IllegalArgumentException as iae:
            Util.exit_with_message(iae.message)
        except ArgumentHandler.NumberOfArgumentsException as nae:
            Util.exit_with_message(nae.message)

    def check_argument_validity(self) -> None:
        if len(self.get_arguments()) != 2:
            raise ArgumentHandler.NumberOfArgumentsException("There must be one argument for the search. You entered {}.".format(len(self.get_arguments())-1))
        conditions = self.get_conditions()
        if len(conditions) != 9:
            raise ArgumentHandler.IllegalArgumentException("You must enter 9 conditions for 9 attributes, seperated with commas. You entered {}.".format(len(conditions)))
        for condition in conditions:
            if not self.condition_is_valid(condition):
                raise ArgumentHandler.IllegalArgumentException("The condition {} is not valid.".format(condition))

    @staticmethod
    def condition_is_valid(condition: str) -> bool:
        return True if re.match(ArgumentHandler.REGEX, condition) is not None else False

    def get_conditions(self) -> list:
        str_conditions = self.get_arguments()[1]
        conditions = str_conditions.split(',')
        return conditions

    def get_arguments(self) -> list:
        return self.__arguments


class DatabaseCleaner:
    """Performs data cleaning process."""

    @staticmethod
    def fun_data_clean(data_dictionary: dict) -> list:
        # I wanted to change the name of the function funDataClean to fun_data_clean to comply with naming conventions.
        all_records = []
        for identity in data_dictionary:
            record = data_dictionary.get(identity)
            all_records.append(record)

        all_missing = []

        for record_index in range(len(all_records)):
            for attr_index in range(0, 9):
                if all_records[record_index][attr_index] == "?":
                    avg = Util.true_round(DatabaseCleaner.get_average_of_knowns(attr_index, all_records, all_records[record_index][-1]))
                    all_records[record_index][attr_index] = avg
                    all_missing.append(avg)

        average_of_all_missing = sum(all_missing) / len(all_missing)

        print('The average of all missing values is  : ' + '{0:.4f}'.format(average_of_all_missing))

        return all_records

    @staticmethod
    def get_average_of_knowns(attribute_index: int, records: list, class_label: str) -> float:
        known_values = []
        for record in records:
            if record[attribute_index] != "?" and record[-1] == class_label:
                known_values.append(int(record[attribute_index]))
        return sum(known_values) / len(known_values) if len(known_values) != 0 else 0


class Conditions:
    ANY = r"\?"
    LESS_THAN_OR_EQUAL_TO = r"<=:\d"
    MORE_THAN_OR_EQUAL_TO = r">=:\d"
    MORE_THAN = r">:\d"
    LESS_THAN = r"<:\d"
    EQUAL_TO = r"=:\d"
    NOT_EQUAL_TO = r"!=:\d"


class Condition:

    def __init__(self, number, condition_regex) -> None:
        self.number = number
        self.regex = condition_regex


class Search:

    def __init__(self, records: list, conditions: list) -> None:
        self.__records = records
        self.__conditions = conditions

    def perform_stepwise_search(self) -> None:
        # I wanted to change the name of the function performStepWiseSearch to perform_stepwise_search to comply with naming conventions.
        # 2nd phase: Retrieving knowledge from WBC dataset

        records_satisfying_their_conditions = []

        for attr_index in range(1, 10):
            records_satisfying_their_conditions.append(self.get_records_satisfying_condition(attr_index,
                                                                                             Search.convert_to_condition(self.get_conditions()[attr_index - 1])))

        unique_records_satisfying_all_conditions = self.get_records()

        for records in records_satisfying_their_conditions:
            unique_records_satisfying_all_conditions = Search.get_intersection(records, unique_records_satisfying_all_conditions)

        malignant_cases = []
        benign_cases = []

        for record in unique_records_satisfying_all_conditions:
            if record[-1] == "benign":
                benign_cases.append(record)
            if record[-1] == "malignant":
                malignant_cases.append(record)

        total = len(benign_cases) + len(malignant_cases)

        if total != 0:
            print('\nTest Results:\n'
                  '----------------------------------------------'
                  '\nPositive (malignant) cases            : ' + str(len(malignant_cases)) +
                  '\nNegative (benign) cases               : ' + str(len(benign_cases)) +
                  '\nThe probability of being positive     : ' + '{0:.4f}'.format(len(malignant_cases) / total) +
                  '\n----------------------------------------------')
        else:
            print('\nTest Results:\n'
                  '----------------------------------------------'
                  '\nPositive (malignant) cases            : ' + str(0) +
                  '\nNegative (benign) cases               : ' + str(0) +
                  '\nThe probability of being positive     : -' +
                  '\n----------------------------------------------')

    def get_records_satisfying_condition(self, attr_index: int, condition: Condition) -> list:
        records = self.get_records()
        if condition.regex == Conditions.ANY:
            return records
        else:
            records_we_want = []
            for record in records:
                if condition.regex == Conditions.LESS_THAN_OR_EQUAL_TO:
                    if int(record[attr_index]) <= condition.number:
                        records_we_want.append(record)
                if condition.regex == Conditions.MORE_THAN_OR_EQUAL_TO:
                    if int(record[attr_index]) >= condition.number:
                        records_we_want.append(record)
                if condition.regex == Conditions.MORE_THAN:
                    if int(record[attr_index]) > condition.number:
                        records_we_want.append(record)
                if condition.regex == Conditions.LESS_THAN:
                    if int(record[attr_index]) < condition.number:
                        records_we_want.append(record)
                if condition.regex == Conditions.EQUAL_TO:
                    if int(record[attr_index]) == condition.number:
                        records_we_want.append(record)
                if condition.regex == Conditions.NOT_EQUAL_TO:
                    if int(record[attr_index]) != condition.number:
                        records_we_want.append(record)
            return records_we_want

    def get_conditions(self) -> list:
        return self.__conditions

    def get_records(self) -> list:
        return self.__records

    @staticmethod
    def get_intersection(list_1: list, list_2: list) -> list:
        return [element for element in list_1 if element in list_2]

    @staticmethod
    def convert_to_condition(str_condition: str) -> Condition:
        if re.match(Conditions.ANY, str_condition) is not None:
            return Condition(None, Conditions.ANY)
        if re.match(Conditions.LESS_THAN_OR_EQUAL_TO, str_condition) is not None:
            return Condition(int(str_condition.split(":")[1]), Conditions.LESS_THAN_OR_EQUAL_TO)
        if re.match(Conditions.MORE_THAN_OR_EQUAL_TO, str_condition) is not None:
            return Condition(int(str_condition.split(":")[1]), Conditions.MORE_THAN_OR_EQUAL_TO)
        if re.match(Conditions.MORE_THAN, str_condition) is not None:
            return Condition(int(str_condition.split(":")[1]), Conditions.MORE_THAN)
        if re.match(Conditions.LESS_THAN, str_condition) is not None:
            return Condition(int(str_condition.split(":")[1]), Conditions.LESS_THAN)
        if re.match(Conditions.EQUAL_TO, str_condition) is not None:
            return Condition(int(str_condition.split(":")[1]), Conditions.EQUAL_TO)
        if re.match(Conditions.NOT_EQUAL_TO, str_condition) is not None:
            return Condition(int(str_condition.split(":")[1]), Conditions.NOT_EQUAL_TO)
        raise ArgumentHandler.IllegalArgumentException("The condition {} is not valid.".format(str_condition))


class Util:

    @staticmethod
    def exit_with_message(message: str) -> None:
        print(message)
        input("Press enter key to terminate...")
        exit(0)

    @staticmethod
    def true_round(number: float) -> int:
        """
        I needed this utility method because built-in round() method
        does not return true values when for instance, 0.5 involves.
        An example would be round(4.5) which returns 4 rather than 5.
        """
        decimal_point = number - int(number)
        return int(number) if decimal_point < 0.5 else int(number + 1)


def main(argument_list: list) -> None:
    records = DatabaseCleaner.fun_data_clean(data_dic)

    argument_handler = ArgumentHandler(argument_list)
    conditions = argument_handler.get_conditions()

    all_records_with_ids = []
    for index in range(len(records)):
        naked_record = records[index]
        record_with_id = [index + 1]
        record_with_id.extend(naked_record)
        all_records_with_ids.append(record_with_id)

    search = Search(all_records_with_ids, conditions)
    search.perform_stepwise_search()


main(arguments)
